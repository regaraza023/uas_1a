/*
   Nama Program : Program Fungsi dan Procedure
   Tgl buat     : 08 februari 2024
   Nama         : Purnamasari siregar
   Nim          : 301230049
   Kelas        : 1A
*/
#include <iostream>
using namespace std;

// Fungsi untuk menghitung nilai akhir
double hitungNilai(double absen, double tugas, double quiz, double uts, double uas) {
    return ((0.1 * absen) + (0.2 * tugas) + (0.3 * quiz) + (0.4 * uts) + (0.5 * uas)) / 2;
}

// Prosedur untuk menampilkan nilai komponen
void tampilkanKomponen(double absen, double tugas, double quiz, double uts, double uas) {
    cout << "Absen = " << absen << " UTS = " << uts << endl;
    cout << "Tugas = " << tugas << " UAS = " << uas << endl;
    cout << "Quiz = " << quiz << endl;
}

// Prosedur untuk menentukan huruf mutu
char tentukanHurufMutu(double nilai) {
    char hurufMutu;

    if (nilai > 85 && nilai <= 100)
        hurufMutu = 'A';
    else if (nilai > 70 && nilai <= 85)
        hurufMutu = 'B';
    else if (nilai > 55 && nilai <= 70)
        hurufMutu = 'C';
    else if (nilai > 40 && nilai <= 55)
        hurufMutu = 'D';
    else if (nilai >= 0 && nilai <= 40)
        hurufMutu = 'E';

    return hurufMutu;
}

int main() {
    double nilai, quiz, absen, uts, uas, tugas;
    char hurufMutu;

    quiz = 40; absen = 100; uts = 60; uas = 50; tugas = 80;

    tampilkanKomponen(absen, tugas, quiz, uts, uas);
    nilai = hitungNilai(absen, tugas, quiz, uts, uas);
    hurufMutu = tentukanHurufMutu(nilai);

    cout << "Huruf Mutu : " << hurufMutu << endl;

    return 0;
}
