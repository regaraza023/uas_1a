/*
   Nama Program : Merubah program nilai untuk lebih dari satu mahasiswa
   Tgl buat     : 08 februari 2024
   Nama         : Purnamasari siregar
   Nim          : 301230049
   Kelas        : 1A
*/
#include <iostream>
#include <vector>
using namespace std;

struct Mahasiswa {
    double absen, tugas, quiz, uts, uas;
    char Huruf_Mutu;
};

int main() {
    int jumlah_mahasiswa;
    cout << "Masukkan jumlah mahasiswa: ";
    cin >> jumlah_mahasiswa;

    vector<Mahasiswa> daftar_mahasiswa(jumlah_mahasiswa);

    for (int i = 0; i < jumlah_mahasiswa; ++i) {
        cout << "Mahasiswa " << i + 1 << endl;

        // Memasukkan nilai masing-masing komponen
        cout << "Absen: ";
        cin >> daftar_mahasiswa[i].absen;
        cout << "Tugas: ";
        cin >> daftar_mahasiswa[i].tugas;
        cout << "Quiz: ";
        cin >> daftar_mahasiswa[i].quiz;
        cout << "UTS: ";
        cin >> daftar_mahasiswa[i].uts;
        cout << "UAS: ";
        cin >> daftar_mahasiswa[i].uas;

        // Menghitung nilai akhir
        double nilai = ((0.1 * daftar_mahasiswa[i].absen) + (0.2 * daftar_mahasiswa[i].tugas) +
                        (0.3 * daftar_mahasiswa[i].quiz) + (0.4 * daftar_mahasiswa[i].uts) +
                        (0.5 * daftar_mahasiswa[i].uas)) / 2;

        // Menentukan huruf mutu berdasarkan nilai
        if (nilai > 85 && nilai <= 100)
            daftar_mahasiswa[i].Huruf_Mutu = 'A';
        else if (nilai > 70 && nilai <= 85)
            daftar_mahasiswa[i].Huruf_Mutu = 'B';
        else if (nilai > 55 && nilai <= 70)
            daftar_mahasiswa[i].Huruf_Mutu = 'C';
        else if (nilai > 40 && nilai <= 55)
            daftar_mahasiswa[i].Huruf_Mutu = 'D';
        else if (nilai >= 0 && nilai <= 40)
            daftar_mahasiswa[i].Huruf_Mutu = 'E';
    }

    // Menampilkan hasil nilai untuk setiap mahasiswa
    for (int i = 0; i < jumlah_mahasiswa; ++i) {
        cout << "Mahasiswa " << i + 1 << endl;
        cout << "Huruf Mutu : " << daftar_mahasiswa[i].Huruf_Mutu << endl;
    }

    return 0;
}
