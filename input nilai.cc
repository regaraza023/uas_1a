/*
   Nama Program : Merubah program user bisa menginput nilai dan merubah prosentasi nilai
   Tgl buat     : 08 februari 2024
   Nama         : Purnamasari siregar
   Nim          : 301230049
   Kelas        : 1A
*/
#include <iostream>
using namespace std;

int main() {
    double nilai, quiz, absen, uts, uas, tugas;
    char Huruf_Mutu;

    // Meminta input dari pengguna
    cout << "Masukkan nilai Absen: ";
    cin >> absen;
    cout << "Masukkan nilai Tugas: ";
    cin >> tugas;
    cout << "Masukkan nilai Quiz: ";
    cin >> quiz;
    cout << "Masukkan nilai UTS: ";
    cin >> uts;
    cout << "Masukkan nilai UAS: ";
    cin >> uas;

    // Meminta input bobot nilai dari pengguna
    double bobot_absen, bobot_tugas, bobot_quiz, bobot_uts, bobot_uas;
    cout << "Masukkan bobot Absen (dalam desimal): ";
    cin >> bobot_absen;
    cout << "Masukkan bobot Tugas (dalam desimal): ";
    cin >> bobot_tugas;
    cout << "Masukkan bobot Quiz (dalam desimal): ";
    cin >> bobot_quiz;
    cout << "Masukkan bobot UTS (dalam desimal): ";
    cin >> bobot_uts;
    cout << "Masukkan bobot UAS (dalam desimal): ";
    cin >> bobot_uas;

    // Menghitung nilai berdasarkan bobot yang diinputkan
    nilai = ((bobot_absen * absen) + (bobot_tugas * tugas) + (bobot_quiz * quiz) + (bobot_uts * uts) + (bobot_uas * uas)) / (bobot_absen + bobot_tugas + bobot_quiz + bobot_uts + bobot_uas);

    // Menentukan huruf mutu berdasarkan nilai
    if (nilai > 85 && nilai <= 100)
        Huruf_Mutu = 'A';
    else if (nilai > 70 && nilai <= 85)
        Huruf_Mutu = 'B';
    else if (nilai > 55 && nilai <= 70)
        Huruf_Mutu = 'C';
    else if (nilai > 40 && nilai <= 55)
        Huruf_Mutu = 'D';
    else if (nilai >= 0 && nilai <= 40)
        Huruf_Mutu = 'E';

    // Menampilkan nilai dan huruf mutu
    cout << "Nilai : " << nilai << endl;
    cout << "Huruf Mutu : " << Huruf_Mutu << endl;

    return 0;
}
